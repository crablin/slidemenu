//
//  SlideMasterScene.swift
//  SlideMenu
//
//  Created by gamalab on 2015/4/23.
//  Copyright (c) 2015年 gamania. All rights reserved.
//

import UIKit

class SlideMasterScene: UIViewController {
    
    @IBInspectable var segueMenuIdentifier : String?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let identifier = segueMenuIdentifier {
            self.performSegueWithIdentifier(segueMenuIdentifier, sender: self)
        }
        
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
