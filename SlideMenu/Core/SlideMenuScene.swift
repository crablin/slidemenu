//
//  SlideMenuScene.swift
//  SlideMenu
//
//  Created by gamalab on 2015/4/20.
//  Copyright (c) 2015年 gamania. All rights reserved.
//

import UIKit

class MenuOption {
    
    let menuWidth: CGFloat = 270.0
    let velocityPanOpen: CGFloat = 300.0
    let velocityPanClose: CGFloat = -100.0
    let contentViewOpacity: CGFloat = 0.5
    let shadowOpacity: CGFloat = 0.3
    let shadowRadius: CGFloat = 5.0
    let shadowOffset: CGSize = CGSizeMake(-3,0)
    let animationDuration: CGFloat = 0.3
    let velocity: CGFloat = 0.5
    
    init() {
        
    }
}

class MenuSegueLinkCell: UITableViewCell {
    @IBInspectable var storyboardName : String?;
    @IBInspectable var sceneIdentifier: String?;
    @IBInspectable var isModalSegue   : Bool = false;
    
}

extension UINavigationController {
    
    func rootViewController() -> UIViewController {
        return self.childViewControllers[0] as! UIViewController;
    }
    
}

class SlideMenuScene: UITableViewController, UITableViewDelegate {
    
    @IBInspectable var segueContentIdentifier : String?;
    
    var options : MenuOption = MenuOption()
    var hasOpened : Bool = false;
    
    var master : UIViewController!
    var contentController : UINavigationController!
    var contentView : UIView!
    var maskView: UIView!
    var currentCell : MenuSegueLinkCell!
    
    var modalController : UINavigationController!
    var prevCell : MenuSegueLinkCell!
    
    var panGesture : UIPanGestureRecognizer!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = true
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.currentCell == nil {
            self.currentCell = self.tableView.cellForRowAtIndexPath(self.initialIndexPath()) as! MenuSegueLinkCell
            
            if let identifier = segueContentIdentifier {
            
                self.performSegueWithIdentifier(segueContentIdentifier, sender: self)
                
            }
        }
        
    }
    
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        
        var frame = self.contentView.frame
        
        self.maskView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    }
    
    
    // MARK: - ContentView and IndexPath
    func initialIndexPath() -> NSIndexPath {
        return NSIndexPath(forRow: 0, inSection: 0)
    }
    
    func sceneContent() -> UINavigationController? {
        
        var content : UINavigationController?
        
        if let sbName = self.currentCell.storyboardName {
            
            var storyboard = UIStoryboard(name: sbName, bundle: nil)
            
            if let sceneID = self.currentCell.sceneIdentifier {
                content = storyboard.instantiateViewControllerWithIdentifier(sceneID) as? UINavigationController
            } else {
                content = storyboard.instantiateInitialViewController() as? UINavigationController
            }
        }
        
        if let destination = content  {
            
            if  self.currentCell.isModalSegue {
                
                var backButton = self.copyBarButtonItem(destination.rootViewController().navigationItem.leftBarButtonItem, targetTitle: "back", actionSelector: "touchUpBack:")
                
                destination.rootViewController().navigationItem.leftBarButtonItem = backButton
                
            } else {
                
                var leftButton = self.copyBarButtonItem(self.master.navigationItem.leftBarButtonItem, targetTitle: "menu", actionSelector: "touchUpMenu:")
                
                
                destination.rootViewController().navigationItem.leftBarButtonItem = leftButton
                
                panGesture = UIPanGestureRecognizer(target: self, action: "handleMenuPanGesture:")
                
                content!.view.addGestureRecognizer(panGesture)
                
                maskView = UIView(frame: destination.rootViewController().view.frame)
                
                maskView.backgroundColor = UIColor.blackColor()
                maskView.hidden = !self.hasOpened
                maskView.alpha = !self.hasOpened ? 0.0 : self.options.contentViewOpacity
                
                var tapGesture = UITapGestureRecognizer(target: self, action: "handleMenuTapGesture:")
                
                maskView.addGestureRecognizer(tapGesture)
                
                destination.rootViewController().view.addSubview(maskView)
            }
            
            
            
        }
        
        return content
        
    }
    
    private func copyBarButtonItem(targetItem: UIBarButtonItem?, targetTitle: String, actionSelector: String) -> UIBarButtonItem {
        
        var resultButton : UIBarButtonItem?
        
        if let item = targetItem {
            if let itemImg = targetItem?.image {
                resultButton = UIBarButtonItem(image: itemImg, style: UIBarButtonItemStyle.Plain, target: self, action: NSSelectorFromString(actionSelector))
            } else {
                resultButton = UIBarButtonItem(title: item.title, style: UIBarButtonItemStyle.Plain, target: self, action: NSSelectorFromString(actionSelector))
            }
            
            resultButton?.tintColor = targetItem?.tintColor
        }
        
        return resultButton ?? UIBarButtonItem(title: targetTitle, style: UIBarButtonItemStyle.Plain, target: self, action: NSSelectorFromString(actionSelector))
    }
    
    struct contentPanState {
        static var frameAtStartOfPan: CGRect = CGRectZero
        static var startPointOfPan: CGPoint = CGPointZero
    }
    
    func handleMenuPanGesture(panGesture: UIPanGestureRecognizer) {
        switch panGesture.state {
        case .Began:
            
            self.addShadowToView(self.contentView)
            
            contentPanState.frameAtStartOfPan = self.contentView.frame
            contentPanState.startPointOfPan = panGesture.locationInView(self.master.view)
            
            self.maskView.hidden = false
            
        case .Changed:
            var translation: CGPoint = panGesture.translationInView(panGesture.view!)
            
            self.contentView.frame = self.applyMenuTranslation(translation, toFrame: contentPanState.frameAtStartOfPan)
            self.maskView.alpha = self.applyContentAlpha(self.contentView.frame.origin)
            
        case .Ended:
            
            var velocity:CGPoint = panGesture.velocityInView(panGesture.view)
            var menuWillClose = !(self.applyMenuIsOpenVelocity(velocity, prevFrame: contentPanState.frameAtStartOfPan))
            
            self.toggleMenu(menuWillClose, velocity: velocity.x, completion: nil)
            
            
        default:break;
        }
    }
    
    func handleMenuTapGesture(tapGesture: UITapGestureRecognizer) {
        self.toggleMenu(self.hasOpened, completion: nil)
    }
    
    @IBAction func touchUpMenu(sender: AnyObject) {
        
        self.toggleMenu(self.hasOpened, completion: nil)
    }
    
    @IBAction func touchUpBack(sender: AnyObject) {
        modalController.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.currentCell = self.prevCell
            
            self.prevCell = nil
        })
    }
    
    func toggleMenu(willClose:Bool, velocity: CGFloat = 0.0, completion: (() -> Void)?) {
        // get menu toggle speed
        var duration = Double(self.options.animationDuration)
        var frame = self.contentView.frame
        
        frame.origin.x = willClose ? 0 : self.options.menuWidth
        
        if (velocity != 0.0) {
            let xOrigin: CGFloat = self.contentView.frame.origin.x
            let finalXOrigin = (willClose ? 0.0 : self.options.menuWidth)
            
            var newDuration = Double(fabs(xOrigin - finalXOrigin) / velocity)
            newDuration = Double(fmax(0.1, fmin(1.0, newDuration)))
            
            if (newDuration < duration) {
                duration = newDuration
            }
            
        }
        
        if (!willClose) {
            self.addShadowToView(self.contentView)
        }
        
        self.maskView.hidden = false
        
        UIView.animateWithDuration(duration, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut,
            animations: { () -> Void in
                
                self.contentView.frame = frame
                self.maskView.alpha = self.hasOpened ? 0.0 : self.options.contentViewOpacity
                
            }) { (isComplete) -> Void in
                
                if (willClose) {
                    self.removeShadow(self.contentView)
                }
                
                self.hasOpened = !willClose

                self.maskView.hidden = !self.hasOpened
                self.maskView.alpha = !self.hasOpened ? 0.0 : self.options.contentViewOpacity
                
                if let fn = completion {
                    fn()
                }
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            
            if cell != self.currentCell {
                
                self.prevCell = self.currentCell
                self.currentCell = cell as! MenuSegueLinkCell
                
                self.performSegueWithIdentifier(segueContentIdentifier, sender: self)
                
            } else {
                
                self.toggleMenu(true, completion: nil);
            }
        }
        
    }
    
    private func addShadowToView(targetContainerView: UIView) {
        targetContainerView.layer.masksToBounds = false
        targetContainerView.layer.shadowOffset = self.options.shadowOffset
        targetContainerView.layer.shadowOpacity = Float(self.options.shadowOpacity)
        targetContainerView.layer.shadowRadius = self.options.shadowRadius
        targetContainerView.layer.shadowPath = UIBezierPath(rect: targetContainerView.bounds).CGPath
    }
    
    private func removeShadow(targetContainerView: UIView) {
        targetContainerView.layer.masksToBounds = true
    }
    
    private func applyMenuTranslation(translation: CGPoint, toFrame:CGRect) -> CGRect {
        
        var newOrigin: CGFloat = toFrame.origin.x
        newOrigin += translation.x
        
        var minOrigin: CGFloat = 0.0
        var maxOrigin: CGFloat = self.options.menuWidth
        var newFrame: CGRect = toFrame
        
        if newOrigin < minOrigin {
            newOrigin = minOrigin
        } else if newOrigin > maxOrigin {
            newOrigin = maxOrigin
        }
        
        newFrame.origin.x = newOrigin
        return newFrame
    }
    
    private func applyContentAlpha(contentOrigin: CGPoint) -> CGFloat {
        
        var contentAlpha: CGFloat = (contentOrigin.x / options.menuWidth) * options.contentViewOpacity;
        
        if contentAlpha < 0 {
            contentAlpha = 0
        } else if contentAlpha > options.contentViewOpacity {
            contentAlpha = options.contentViewOpacity
        }
        
        return contentAlpha
    }
    
    private func applyMenuIsOpenVelocity(velocity: CGPoint, prevFrame:CGRect) -> Bool {
        
        var status :Bool
        
        if (self.hasOpened) {
            status = (velocity.x < self.options.velocityPanClose) ? false : true
        } else {
            status = (velocity.x > self.options.velocityPanOpen) ? true : false
        }
        
        return status
    }
    
    
}