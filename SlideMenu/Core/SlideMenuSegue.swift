//
//  SlideMenuSegue.swift
//  SlideMenu
//
//  Created by gamalab on 2015/4/23.
//  Copyright (c) 2015年 gamania. All rights reserved.
//

import UIKit

class SlideMenuSegue: UIStoryboardSegue {
    
    override func perform() {
        
        var master = self.sourceViewController as! SlideMasterScene
        var menu = self.destinationViewController as! SlideMenuScene
        
        var frame = master.view.frame
        
        menu.master = master
        
        master.addChildViewController(menu)
        master.view.addSubview(menu.view)
    }
    
    
}