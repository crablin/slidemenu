//
//  SlideContentSegue.swift
//  SlideMenu
//
//  Created by gamalab on 2015/4/20.
//  Copyright (c) 2015年 gamania. All rights reserved.
//

import UIKit

class SlideContentSegue: UIStoryboardSegue {
    override init!(identifier: String!, source: UIViewController, destination: UIViewController) {
        
        var menu = source as! SlideMenuScene;
        
        super.init(identifier: identifier,
            source: source,
            destination: menu.sceneContent()!)
    }
    
    override func perform() {
        
        var menu = self.sourceViewController as! SlideMenuScene
        var content = self.destinationViewController as! UINavigationController
        
        if menu.currentCell.isModalSegue {
            menu.toggleMenu(true, completion: { () -> Void in
                
                content.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
                menu.presentViewController(content, animated: true, completion: nil)
                
                menu.modalController = content
            });
        }
        else {
            
            var frame = menu.view.frame;
            
            if let prevView = menu.contentView {
                content.view.frame = prevView.frame
                prevView.removeFromSuperview()
            }
            
            if let prevController = menu.contentController {
                prevController.removeFromParentViewController()
            }
            
            menu.contentController = content
            menu.contentView = content.view
            
            menu.master.addChildViewController(content)
            
            content.navigationController?.navigationItem.leftBarButtonItems = menu.navigationController?.navigationItem.leftBarButtonItems
            
            menu.master.view.addSubview(content.view)
            
            if (menu.hasOpened) {
                menu.toggleMenu(true, completion: nil);
            }
        }
    }
}
