//
//  MenuViewController.swift
//  SlideMenu
//
//  Created by gamalab on 2015/4/20.
//  Copyright (c) 2015年 gamania. All rights reserved.
//

import UIKit

class MenuViewController: SlideMenuScene, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        super.tableView(tableView, didSelectRowAtIndexPath: indexPath)
    }
    
    @IBAction func unwindToMenu(segue:UIStoryboardSegue) {
        
    }

}
